
const express = require("express");
const app = express();

const ROUTES = {
	"seydlitz.cc": "http://seydlitz.cc:8080",
	"admin.seydlitz.cc": "http://seydlitz.cc:8080",
	"mousai.seydlitz.cc": "http://seydlitz.cc:8081",
	"nas.seydlitz.cc": "http://seydlitz.cc:8082"
};

app.get("/*", (req, res) => {
	console.log(req);
	const host = req.headers["host"];
	if (!host) {
		res.status(400);
		return;
	}
	const route = ROUTES[host];
	if (!route) {
		res.status(404);
		return;
	}

	res.redirect(route + req.originalUrl);
});

app.listen(8000, () => {
	console.log("listening");
});
